Individual Repository for DevOps

This repository contains the files for all the class assignments of DevOps.

- [Class Assignment 1](https://bitbucket.org/BVSousa90/devops-20-21-1201761/src/master/ca1/)

- [Class Assignment 2, Part 1](https://bitbucket.org/BVSousa90/devops-20-21-1201761/src/master/ca2/Part_1/)

- [Class Assignment 2, Part 2](https://bitbucket.org/BVSousa90/devops-20-21-1201761/src/master/ca2/Part_2/)

- [Class Assignment 3, Part 1](https://bitbucket.org/BVSousa90/devops-20-21-1201761/src/master/ca3/Part_1)

- [Class Assignment 3, Part 2](https://bitbucket.org/BVSousa90/devops-20-21-1201761/src/master/ca3/Part_2)

- [Class Assignment 4](https://bitbucket.org/BVSousa90/devops-20-21-1201761/src/master/ca4/)

- [Class Assignment 5,Part 1](https://bitbucket.org/BVSousa90/devops-20-21-1201761/src/master/ca5/Part_1)

- [Class Assignment 5,Part 2](https://bitbucket.org/BVSousa90/devops-20-21-1201761/src/master/ca5/Part_2)
