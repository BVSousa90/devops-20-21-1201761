# Class Assignment 2 Report, Part 2 - Build Tools with Gradle

# 1.Analysis, Design and Implementation

A brief introduction to *Gradle* was made
in [ca2, part1](https://bitbucket.org/BVSousa90/devops-20-21-1201761/src/master/ca2/Part_1/), how it works, and some of
its features.

The goal for this *Part 2*, was to convert
the [basic version](https://bitbucket.org/BVSousa90/devops-20-21-1201761/src/master/ca1/tutorial/tut-basic/), of the
Tutorial application to *Gradle* instead of *Maven*.

### Implementation

The following steps were made in order to complete this assignment.

First was created a new folder for this second part of the assignment, *Part_2*, then a new branch using _git checkout
-b part2_.

Then it was necessary to use [Spring Initializer](https://start.spring.io), where we generated all the necessary spring
project files. In this project the settings were:

> - Language: java
> - Spring Boot: 2.4.5
> - Packaging: jar
> - Dependencies: Rest Repositories, Thymeleaf, Spring Data JPA and H2 Database.

After moving the new project to the recent folder *Part_2*, it was necessary to delete the *src* (using _rm -rf src_)
folder and place the *src* folder
from [basic version](https://bitbucket.org/BVSousa90/devops-20-21-1201761/src/master/ca1/tutorial/tut-basic/),
additional it was also necessary to copy *webpack.config.js* and *package.json*. In case you have the *built* folder
in *resources*, you have to delete it since this folder should be generated from the javascript by the tool webpack.

Now is possible to run the application, as Spring Boot, using _./gradlew bootRun_. Using _./gradlew tasks_ is possible
to view the available _gradle tasks_.

If we try to open our browser on _http://localhost:8080_, we can see that it's now working yet. Because the plugin
necessary, in Gradle, for dealing with the frontend code is not in our script yet. So in order to fix this is necessary
to add, to _build.gradle_:

```console
id "org.siouan.frontend" version "1.4.1" //the plugin
----------
frontend {
   nodeVersion = "12.13.1"          
   assembleScript = "run webpack"
}
----------
"scripts": {
  "watch": "webpack --watch -d",
  "webpack": "webpack"
 }
```

The second part, is used for configure the plugin. And the last part is for updating the scripts section in package.json
to configure the execution of webpack.

Using _./gradlew build_ followed by _./gradlew bootRun_, and trying to open the localhost once again, is possible to see
some information.

*2. Create a new task: Copy jar file*

This task allows to create a backup directory to save a copy of the generated jar file. Add the following lines to _
build.gradle_.

```console

task copyJar(type: Copy){
	group = "DevOps"
	description = "Copy jar file in build/libs to dist"
	from(file('build/libs/'))
	into('dist')
}
```

To run this task _./gradlew copyJar_, since the _dist_ folder didn't exist ,after running the command is now possible to
visualize a new folder same level as the *src folder*.

*3. Create a new task: Delete all the files generated by webpack*

This task is an addiction to the clean task, which is a default task from *Gradle*. In this case, the task clean is
going to incorporate the new task.

```console

task deleteBuilt(type: Delete){
	group = "DevOps"
	description = "Delete build folder"
	delete "${rootDir}/src/main/resources/static/built"
}

clean.configure{dependsOn(deleteBuilt)}

```

When we use _./gradlew clean_, the task clean depends on the task _deleteBuilt_. It is also possible to use _./gradlew
--console=plain clean_.

All the files in the _built_ folder and the folder itself, were deleted when running the previous command.

*4. Merge the branch with master*

 ```console
 git checkout master
 git merge part2
 git push
 ```

After all the tasks are done, we need to merge the new branch into the master.

In this first part of the assigment, there were some difficulties, since in the beginning the project wasn't building
successfully, because the folder _tests_ in _src_ was deleted, and the test classes were in the _main_ folder of _src_
causing some troubles in the building process.

# 2.Analysis of an Alternative

The alternative that was chosen for this assignment was _Ant (Another Neat Tool)_.

_Ant_ is a Java-based build tool from software development company _Apache_. The main known usage of _Ant_ is the build
of Java applications, for instance _C_ or
_C++_ applications. More generally, _Ant_ can be used to pilot type of process, that can be described as targets and
tasks.

Some of _Ant_ features are:

> - It is platform neutral and can handle platform specific properties, such as file separators.
> - Is good at automating complicated repetitive tasks.
> - Comes with a big list of predefined tasks;
> - Between others.

# 3. Implementation of the Alternative

In order to use _Ant_, we first created a project using _Maven_, the process was the same already described in section
1, _Implementation_, but instead of choosing _Gradle_, we chose _Maven_. The folder _src_ was also deleted and replaced
with
_src_ from _ca1_, and the same files were copied into the new folder _Part_2/Alternative/demo_.

Then it was necessary to install _Maven_ (_sudo apt install Maven_) and _Ant_ (_sudo apt install Ant_). To ascertain
that the tools were correctly installed the commands _mvn --version_ and _ant -version_ were used.

To work on this implementation a new branch was created, _git checkout -b antImplementation_.

*1. Migration of the Maven project to Ant*

```
mvn ant:ant 
```

This command was used in order to move the _Maven_ project into _Ant_ build environment. Using this command some files
were generated :

```
build.xml
maven-build.xml
maven-build.properties
```

In order to see which are the main project targets, we use _ant -projecthelp_.

Before compiling the project, the _maven-build.xml_ needs to be altered, in order to be the same as the Java version:

```
target=1.8
source=1.8
```

Then it's possible to compile the project using _ant compile_.

Before initializing _Ant_, first we must delete all files/folders related to _Maven_:

```
pom.xml
mvnw
mvnw.cmd
./mvn
```

After this is now possible to initialize _Ant_, using _ant buildfile build.xml_. Running the _Spring Boot_ through
Intellij (_ReactAndSpringDataRestApplication_), and opening the localhost(8080), is possible to see that the information
that was expected is there.

*2. Create a new task: Copy jar file*

In the build.xml file we add the following lines:

```console
<target name="copyJar">
    <copy todir="dist" file="target/demo-0.0.1-SNAPSHOT.jar"/>
 </target>
```

To run this new target:

```console       
ant copyJarFile
```

Like it was explained in the section 1.2, this task will create a backup directory to save the generated jar file.

*3. Create a new task: Delete all the files generated by webpack*

```console

 </target>
  <target name="deleteBuilt">
    <delete dir="src/main/resources/static/built"/>
  </target> 
  //This target was moved from the maven-build.xml file
  <target name="clean" description="Clean the output directory" depends="deleteBuilt">
    <delete dir="${maven.build.dir}"/>
  </target>

```
Like before, in section 1.3, the task _clean_ depends on the task _deleteBuilt_.
We can run this tasks using _ant deleteBuilt_ and _ant clean_.
And like before the folder _built_ was deleted when running the command. 

*4. Merge the branch with master*

In the end of the implementation the new branch needs to be merged with master and the final version of the 
class assignment needs to be tagged, using: 

```console
git checkout master
git merge antImplementation

git tag -a ca2-part_2 -m "Ca2-part_2 final version"
git push origin ca2-part2
```


There were some difficulties with the alternative implementation, starting the _Spring Boot_ through the 
command line wasn't possible, maybe because the project wasn't an _Ant_ project, and suffer the migration from a _Maven_ one. 


##Comparing Gradle and Ant 

In my opinion, and overall, _Gradle_ is a more user-friendly tool and has more flexibility than _Ant_.

_Ant_ is now quite an old-fashioned build tool, and uses _XML_, while _Gradle_ is a _groovy-based DSL_ which makes 
writing _Gradle_ build scripts easier than _Ant_ scripts.
