# Class Assignment 5 Report Part 2 - CI/CD Pipelines with Jenkins

## 1. Analysis, Design and Implementation

A brief introduction to _Jenkins_ was already made in [CA5,Part_1](https://bitbucket.org/BVSousa90/devops-20-21-1201761/src/03eb87c5ec25658f655f922f3caf8b3f9b2d0c73/ca5/Part_1).

## Implementation

The goal for this second part of the assignment was to create a pipeline in _Jenkins_ to build the spring boot application, developed in [CA2,Part_2](https://bitbucket.org/BVSousa90/devops-20-21-1201761/src/03eb87c5ec25658f655f922f3caf8b3f9b2d0c73/ca2/Part_2).

Since a pipeline was already created in the first part, some steps aren't necessary such as creating an acccount or adding the _bitbucket_ credentials.
Also a new pipeline was created for this part, the same way as previously explained.

_1.Changing the gradle.build_

First I created a folder _Part_2_ inside _ca5_ and with a copy of the application from _ca2,part_2_.

Then in order to build a _War_ file, it was necessary to add somethings to the _gradle.build_ script:

```
 plugins{
   (...)
   id 'war'
}

dependencies{
(...)
    // To support war file for deploying to tomcat
	providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'
}

```

After this, the changes were comitted to the repository.

_2.Creating the Dockerfile and Jenkinsfile_

First it was necessary to create a _Dockerfile_ in order to generate a docker image with _Tomcat_ and my _War_ file.

```
FROM tomcat

RUN apt-get update -y

RUN apt-get install -f

RUN apt-get install git -y

RUN apt-get install nodejs -y

RUN apt-get install npm -y

RUN mkdir -p /tmp/build

RUN ls

ADD /build/libs/tut-basic-ca2-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/ <-- the path to my _War_ file

EXPOSE 8080
```

After creating this file, it was pushed to my personal repository inside the _demo_ folder.

For this part, the _Jenkisfile_ suffered some modifications. Such as the working directory, the addition of a new stage and a new command in the _Assemble_ stage.

```
pipeline {
    agent any
    stages {
        stage('Checkout') {
           (...)
     }

        stage('Assemble') {
            steps {
                echo 'Assembling...'
                dir('ca2/Part_2/demo/') {
                     sh './gradlew clean bootJar'
		             sh './gradlew clean bootWar' <-- Generating the War file
                }
            }
        }


        stage('Test') {
            (...)
         }

        stage('Javadoc'){
           (...)
         }

        stage('Docker Image'){
            steps{
                echo 'Building and pushing Image...'
                dir('ca5/Part_2/demo'){
                    sh './gradlew clean build'
                    script{
                        def customImage = docker.build("1201761/devops2021:${env.BUILD_NUMBER}") <--the name of my repository and the variable in order to tag the image

                        docker.withRegistry('', '1201761'){ <-- credentials ID to access docker hub
                            customImage.push()
                        }
                    }
                    sh "docker rmi 1201761/devops2021:${env.BUILD_ID}" <-- in order to remove the docker image locally
                }
            }
        }


        stage('Archiving') {
            (...)
    }
 }
```

Some stages were purposefully omitted since they didn't suffer any change (except for the working directory) regarding _Jenkinsfile_ from the first part.

In this file, the _Docker Image_ stage, is new and it was where an image was generated using the _Dockerfile_ and was pushed to my personal _Docker Hub_ repository, so it was also necessary to add the _Docker Hub_ credentials to _Jenkins_ the same way as the _Bitbucket_ credentials were added in the first part.

After this step, and pushing the file to my repository, a new pipeline was created with the name "_ca5,Part2_", using this _Jenkinsfile_.

_3.Installing plugins and fixing docker related errors_

There were some failed builds and the main issues were missing plugins and the docker-jenkins connection.

It was necessary to install three plugins:

> - Docker: In order to integrate Jenkins with Docker;

> - Docker-Pipeline: In order to build and use Docker containers from pipelines.

> - docker-build-step: a plugin that allows to add different docker commands to a job as build steps.

In order to push the image, _Docker_ had to be running in my local machine, it took sometime in order to understand where the problem was, since it was also necessary to add _Jenkins_ as group user of _Docker_ in order to establish a connection successfully.

After the first successful build it was possible to see the new image in the _Docker Hub_ repository.

The new image is tagged as "2" since it was the second build in _Jenkins_.
![docker_hub](./images/1.png)

The builds made in the new pipeline.
![builds](./images/2.png)

Ending the first part of this assignment.

## 2. Analysis of an Alternative

### Buddy - The DevOps Automation PLatform

The alternative that was chosen for this assignment was [_Buddy_](https://buddy.works/).
It's a web-bases and self-hosted continuous integration and delivery software for _Git_ developers that can be used to build, test and deploy web sites and applications with code from _GitHub_, _Bitbucket_ and _GitLab_. It employs _Docker_ containers with pre-installed languages and frameworks for builds.

The service supports over 30 pre-configured actions that can be modified, since _Amazon Web Services_ to _Google Services_ actions.

## 3. Implementation of the Alternative

Before starting to work on _Buddy_, it's necessary to create an account, it's possible to enter using the _Bitbucket_ credentials, so _Buddy_ has full access to all the projects/repositories that you may have in your account.
In this case, devops repository was the chosen one. The code was automatically import so there was no need for a _Checkout_ stage.

_Create a pipeline and his actions_

For this project, a pipeline named "_Ca5_Alternative_" was created, where was indicated the trigger mode, manual in this case, and the branch that were to trigger the pipeline.

![pipeline](./images/4.png)

Creating the stages in _Jenkins_ is equivalent to adding actions in _Buddy_. So in this case eight actions were created and one more in order to send the archives to my email, but this step was not entirely needed.
For each action the working directory had to be specified.

![actions](./images/3.png)

Through _Assemble_ until the _Javadocs_ action the commands that were used were the same as the _Jenkinsfile_ since it was possible to use gradle.
For the Buuilding and pushing the docker image stage, first it was necessary to add a new integration to _Buddy_.

![docker_integration](./images/5.png)

It was necessary to introduce the username and password of my personal _docker hub_ account.

In this action it was also necessary to give the path and context of the _Dockerfile_ that was already created in step 2 of the first part of this report.

![docker_action](./images/6.png)

It was also important to chose the _docker hub_ account and the repository where the image was going to be pushed.
![registry](./images/7.png)

Going to the _docker hub_ repository it was possible to confirm that the image was indeed pushed (3 and 4).
![rep](./images/8.png)

The last actions were to archive into created folders the test reports and javadocs. A disvantage of using _buddy_.

When running the pipeline it was possible to choose which actions was going to be active in each execution or simultaneously.

## 4. Jenkins vs Buddy

The first difference between these tools is that _Buddy_ belongs to "Continuous Deployment" category while _Jenkins_ can be primarily classified under "Continuous Integration".

_Buddy_ is a more user friendly tool and has access to a various range of actions and services, it's also directly connected to the repository and has _Docker_ integrated which makes it easier to build and push _docker_ images.

However unlike _Jenkins_, it's not possible to access the tests graphics and the archiving step wasn't performed in _Buddy_. until the _Javadocs_ action the commands that were used were the same as the _Jenkinsfile_ since it was possible to use gradle

Before choosing _Buddy_, I investigated some other alternatives such as _Codefresh_ and _TravisCI_ and even _Bitbucket_ pipelines. I concluded that are a lot of alternatives to _Jenkins_ some more user friendly and more easy to work with, however I found working with _Jenkins_ quite pleasant.
