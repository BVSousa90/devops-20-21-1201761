# Class Assignment 5 Report Part 1 - CI/CD Pipelines with Jenkins

## 1. Analysis, Design and Implementation

## Jenkins

Is a free and open source automation server. It helps automate the parts fo software development related to building, testing and deploying, facilitating continous integration and continuous delivery.

It's distributed as a _WAR_ archive and as installer packages for the major operating systems, as a Homebrew package, as a Docker image and as source code. The source code is mostly _Java_, with a few _Groovy_, _Ruby_ and _Antlr_ files.

You can run the _Jenkins WAR_ standalone or as a servlet in a Java application server as _Tomcat_. In either way it produces a web user interface and accepts calls to its _REST API_.

Running _Jenkins_ for the first time, it creates an administrative user with a long random password, which you can paste into its initial webpage to unlock the installation.

### Pipelines

Is a suite of plugins which supports implementing and integrating continous delivery pipelines into _Jenkins_.

_Jenkins_ pipelines can be declarative or scripted. A declarative pipeline, the simpler of the two, uses _Groovy-compatible_ syntax. A declarative pipeline starts with a pipeline block, defines an agent(defines where you want to run the pipeline), and defines stages(contains a sequence of one or more stage directives) that include executable steps.

The definition of a _Jenkins_ Pipeline is written into a text file, _Jenkinsfile_, which in turn can be committed to a project's source control repository.
Creating a _Jenkinsfile_ and committing it to source control provides a number of immediate benefits:

> - Automatically creates a Pipeline build process for all branches and pull requests;
> - Code review/iteration on the Pipeline;
> - Audit trail for Pipeline;
> - Single source of truth for the Pipeline, which can be viewed and edited by multiple members of the project.

## Implementation

The goal for this first part, was to practice with _Jenkin_s using the _"gradle basic demo"\_ project,[ca2-part1](https://bitbucket.org/BVSousa90/devops-20-21-1201761/src/master/ca2/Part_1/).

After installing _Jenkins_ and creating an account it's possible to start this class assignment.

_1.Add Bitbucket credentials to Jenkins_

The first stage of _Jenkinsfile_ is _Checkout_ to a git repository, for that is necessary the credentials of the repository.
In order to add the _Bitbucket_ credentials to _Jenkins_ we need to go to:

> Dashboard -> Manage Jenkins -> Security -> Manage Credentials -> Jenkins(global) -> Add credential

Its necessary to insert the password and user of _bitbucket_ and then a credentials ID in order to use it in the _Jenkinsfile_.

_2.Create Jenkinsfile_

Before starting the new file I created a new folder inside _ca5_, named _Part_1_, which will _Jenkinsfile_.
In order to create this file, I first looked at some examples that were provided during class and in the class materials.

```puml
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'devops-bitbucket-credenciais', url: 'https://BVSousa90@bitbucket.org/BVSousa90/devops-20-21-1201761.git'
        }
     }

        stage('Assemble') {
            steps {
                echo 'Assembling...'
                dir('ca2/Part_1/gradle_basic_demo/') {
                    script{
                        sh './gradlew clean jar'
                    }
                }
            }
        }

        stage('Test') {
            steps {
                dir('ca2/Part_1/gradle_basic_demo/') {
                    echo 'Testing...'
                    script {
                       sh './gradlew test'
                    }
                    junit '**/test-results/**/*.xml'

                }
            }
        }

        stage('Archiving') {
            steps {
                echo 'Archiving...'
                dir('ca2/Part_1/gradle_basic_demo/') {
                    archiveArtifacts 'build/libs/*'
                }
            }
        }
    }
 }


```

In this case, my file has four different stages:

> -Checkout (access ny personal repository, where _credentialsId_ is the ID that I gave my credentials)

> -Assemble (compiles and produces the archive files with the application. Since usign the _build_ task was also going to execute the tests, I used the _clean_ task)

> -Test (executes the Unit tests, publishs them in _Jenkins_ and stores them in _test-results_)

> -Archiving (archives in _Jenkins_ the archive files that were generated during _Assemble_)

Each stage contains steps, and inside these steps are:

> -the working directory

> -script (the necessary commands in order to accomplish the purpose of the respective stage)

_3.Create a new Pipeline_

After this is possible to create a _Pipeline_:

> Jenkins Dashboard -> New Item -> Enter an item name -> Pipeline -> OK

In my case, the item name was _CA5_Part_1_. Then is necessary to configure some settings.

In this step, _General_ is not necessary to select any option.
![settings_pipeline](./images/1.png)
In _Pipeline_ I needed to to put my personal repository URL, my _Bitbucket_ credentials and the _Jenkinsfile_ path.
![settings1_pipeline](./images/2.png)

After this we are ready to go.

Before a successfull build there were some failures, because my _gradle wrapper_ was not on my personal repository, so the it wasn't possible to find _gradlew_. After I pushed this _wrapper_ to my _ca2, Part_1_, Success!
