# Class Assignment 4 Report - Containers with Docker

## 1. Analysis, Design and Implementation

## *Docker*

_Docker_ is a platform that allows you to "build, ship, and run any app, anywhere". Is now considered a standard way of
solving one of the costliest aspects of software , deployment.

Everything goes through a common pipeline to a single output that can be used on any target, there's no need to keep
using various tools.

With _Docker_, the configuration effort is separated from the resource management, and the deployment effort is trivial,
all it takes is a simple command, and the environment image is pulled down and ready to run, consuming few resources and
contained so that it doesn't interfere with other environments.

### *Benefits of using _Docker_*

- Replacing VM's;
- Prototyping Software;
- Packaging Software;
- Enabling a Microservices Architecture;
- Modeling Networks;
- Reducing Debugging Overhead;
- Documenting Software Dependencies and Touchpoints;
- Enabling Continuous Delivery;

### *Images and Containers*

They're probably the most important concepts in _Docker_. In the same way a process can be seen as an "application being
executed", a _Docker_ container can be viewed as a Docker image in execution. Another way to look at images and
containers is to view images as classes and containers as objects. In the same way that objects are concrete instances
of classes, containers are instances of images. It's possible to create multiple containers from a single image, and
they are all isolated from one another.

### *Dockerfile*

Is a text file with a series of commands in it. You begin the file by defining the base image with the _FROM_ command.
Next, you declare the maintainer with the _LABEL_ command. This isn't required to make a working _Docker image_ but it's
good practice to include. Next, use the _RUN_ command to clone the todo app code. _WORKDIR_ to move to a new
directory. _EXPOSE_, specifies that containers from the built image should listen on a port and _CMD_, specifies which
command will be run on a startup.

### *Key Docker Commands*

Some of the principal _Docker_ subcommands that are used on a host computer.

> _docker build_ --> Build a Docker image

> _docker run_ --> Run a Docker image as a container

> _docker commit_ --> Commit a Docker container as an image

> _docker tag_ --> Tag a Docker image

> docker info -->  Displays how many containers are stopped, how many are running, how many images between other informations

> docker images --> Displays all the images that exist in your machine

> docker ps --> Allows us to see the containers that are running

> docker exec -it <containerID> /bin/bash --> Allows us to enter a container, and access their folders

## *Implementation*

The main goal for this assignment was to use _Docker_ to set up a containerized environment to execute our solution of
the gradle version of spring basic application.

In order to start this class assignment it was necessary to install _Docker Engine for Linux. This process was quite
simple and fast, only a couple of commands were needed. After this it was possible to start the _class assignment 4_.

*1. Create Dockerfile*
The first step is to go to the folder for this class assignment and create two folders, _web_ and _db_.

> - cp IdeaProjects/devops-20-21-1201761/ca4
> - mkdir web
> - mkdir db

Inside each folder create a new file, name it _Dockerfile_. For this _ca_, a [repository]() was shared that contained
similar examples to ours. A container for the _Tomcat_ and _Spring application_. And another container for running _H2
database_. So it was possible to explore the different files and use them as references.

For the database dockerfile, I used an _Ubuntu_ image.

```
FROM ubuntu

RUN apt-get update && \
  apt-get install -y openjdk-8-jdk-headless && \
  apt-get install unzip -y && \
  apt-get install wget -y

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app/

RUN wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar

EXPOSE 8082
EXPOSE 9092

CMD java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists
```

And for the application, a _Tomcat_ image.

```
FROM tomcat

RUN apt-get update -y

RUN apt-get install -f

RUN apt-get install git -y

RUN apt-get install nodejs -y

RUN apt-get install npm -y

RUN mkdir -p /tmp/web

WORKDIR /tmp/web/

RUN git clone https://BVSousa90@bitbucket.org/BVSousa90/devops-20-21-1201761.git

WORKDIR /tmp/web/devops-20-21-1201761/ca3/Part_2/demo

RUN ./gradlew clean build

RUN cp build/libs/tut-basic-ca2-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

EXPOSE 8080
```

For the _Spring application_ it was necessary to install _git_, _nodejs_ and _npm_. It was also necessary to
_RUN_ _git clone_ in order for my personal repository, namely the folder _ca3/Part_2/demo_, to be cloned into the _
docker container_.

*2.Create docker-compose.yml*

Inside folder _ca4_, another file was created, _docker-compose.yml_:

```
version: '3'
services:
  web:
    build: web
    ports:
      - "8080:8080"
    networks:
      default:
        ipv4_address: 192.168.33.10
    depends_on:
      - "db"
  db:
    build: db
    ports:
      - "8082:8082"
      - "9092:9092"
    volumes:
      - ./data:/usr/src/data
    networks:
      default:
        ipv4_address: 192.168.33.11
networks:
  default:
    ipam:
      driver: default
      config:
        - subnet: 192.168.33.0/24
```

With this file,it is possible to manage various services, build images from _Dockerfiles_, create bridge networks for
the containers. Bridge networks allows us The field _volumes_, allows us to establish a connection between a folder in a
container and a folder in the local machine, where the files present in one side would also be present in the other.
Typically, it is used with databases, to have the changes made in a container persisting between different runs. The _
networks_, is also an interesting aspect of this file, since it's created in order to simulate the IP that is assigned
to the database in the application's properties.Since with containers this IP, would be different. This way we make a
connection between the containers, application and database.

*3.Running docker-compose*

After all files are edited, we can now run, inside the _ca4_ folder, the following command:

> - docker-compose up

This is going to build the images that were in the _Dockerfiles_, clone the folder of my personal repository, create a
container and start the _spring application_. We can check to see if the container is running using:

> - docker ps

    CONTAINER ID   IMAGE     COMMAND                  CREATED       STATUS         PORTS                                                                                  NAMES
    4497ee6bdd80   ca4_web   "catalina.sh run"        3 hours ago   Up 9 seconds   0.0.0.0:8080->8080/tcp, :::8080->8080/tcp                                              ca4_web_1
    5fb7b8c2fe88   ca4_db    "/bin/sh -c 'java -c…"   5 hours ago   Up 9 seconds   0.0.0.0:8082->8082/tcp, :::8082->8082/tcp, 0.0.0.0:9092->9092/tcp, :::9092->9092/tcp   ca4_db_1

If we open our browser and use, _localhost:8080/tut-basic-ca2-0.0.1-SNAPSHOT/_, and we can see the expected result.

![Browser](./images/2.png)

We can also use,_localhost:8080/tut-basic-ca2-0.0.1-SNAPSHOT/h2-console, and access the _H2_ login console.

![H2](./images/3.png)

After replacing the _URL_, for the one seen in the picture above, we can make the connection and access the database.

![H2-table](./images/4.png)

*4.Push Docker images to _Docker Hub_*

The last task of this assignment wast to publish our images to _Docker Hub_. For this step the following commands were
used:

> - docker-compose images To check name of the images that were built.
> - docker tag <name of the image> dockerID/rep_name:tag
> - docker push dockerID/rep_name:tag
>
> - docker login
> - docker tag web_image 1201761/devops1201761:docker_web_image
> - docker push 1201761/devops1201761:docker_web_image

The same process was made for the database image.

![dokcer_hub](./images/9.png)

Concluding the first part of this assignment.

## 2. Analysis of an Alternative

### *Kubernetes*

Is a portable, extensible, open-source platform for managing containerized workloads and services, that facilitates both
declarative configuration and automation.
_Kubernetes_ allows us to automatically mount a storage system of our choice, such as local storages. You can describe
the desire state for the deployed containers, and it can change the actual state to the desired at a controlled rate.
It's possible to automate in order to create new containers for the deployment, to remove existing containers and adopt
all their resources to a new container. It also restarts containers that fail, replaces them, and kills the ones that
don't respond to the user-defined health check. Between other features.

#### *Pods*

Are the smallest deployable units of computing that you can create and manage in _Kubernetes_. Is a group of one or more
containers, with shared storage and network resources, and a specification for how to run the containers. In terms of _
Docker_ concepts, a _Pod_ is similar to a group of _Docker_ containers with shared namespaces and shared filesystem
volumes.

## 3. Implementation of the Alternative

In order to deploy my containers to _Kubernetes_, I used three tools: [_
Minikube_](https://minikube.sigs.k8s.io/docs/start/#debian-package), [_
Kompose_](https://kubernetes.io/docs/tasks/configure-pod-container/translate-compose-kubernetes/) and [_
Kubectl_](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/#install-using-native-package-management).

During this part of the assignment I encountered various problems. The first time I tried to deploy my containers,with
the _docker-compose_ and the _application.properties_ from the first part, it wasn't successful, since one of the tools
I was using was having problems with the networks in the services. So it wasn't possible to deploy. In order to fix this
problem, I had to edit the _docker-compose_, and the _application.properties_ file.

*1.Change the _application.properties_*

I created a new folder, named _Kubernetes_ and added a copy of the application of _ca3,part2_. Inside this folder, it
was also a copy of each _Dockerfile_, web and db, and the _docker-compose_ file. After the problem I encountered I
started again by changing the _spring datasource url_.

```
server.servlet.context-path=/tut-basic-ca2-0.0.1-SNAPSHOT
spring.data.rest.base-path=/api
spring.datasource.url= jdbc:h2://db:9092 <--- New url
#spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE <--Old url
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
# So that spring will no drop de database on every execution.
spring.jpa.hibernate.ddl-auto=update
spring.h2.console.enabled=true
spring.h2.console.path=/h2-console
spring.h2.console.settings.web-allow-others=true
```

The new _url_, has the reference to the database service in the _Dockerfile_, _db_. After this change, the file has to
be pushed to the _bitbucket_ repository.

*2.Change _web Dockerfile_, build new images*

Since a change was made in the application, I needed to also change the _dockerfile_ that was cloning my personal
repository.

```
FROM tomcat

RUN apt-get update -y

RUN apt-get install -f

RUN apt-get install git -y

RUN apt-get install nodejs -y

RUN apt-get install npm -y

RUN mkdir -p /tmp/web

WORKDIR /tmp/web/

RUN git clone https://BVSousa90@bitbucket.org/BVSousa90/devops-20-21-1201761.git

WORKDIR /tmp/web/devops-20-21-1201761/ca4/Kubernetes/demo

RUN ./gradlew clean build

RUN cp build/libs/tut-basic-ca2-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

EXPOSE 8080
```

In this file, the change was made in order to access the new _application.properties_ file that was in the folder _demo_
, inside the folder _Kubernetes_. After this modification, it was important to build new images and also to publish them
in _Docker Hub_. A new image of _db_ wasn't necessary because it didn't suffer any changes, but in my case, I published
two images. For this step I used the command : _docker build -t kubernetes_web_image ._ and _docker build -t
kubernetes_db_image ._
This command builds an image from the _dockerfile_, and gives them a desired name. After this I used the same commands
as in step 4 of the first part.

![alternative_dockerHub](./images/10.png)

*3.Change _docker-compose_ file*

The last step before deploy was to change the _docker-compose_ file.

```
version: '3'
services:
  web:
    image: 1201761/devops2021:alternative_web_image
    depends_on:
      - "db"
    ports:
      - "8080:8080"
  db:
    image: 1201761/devops2021:alternative_db_image
```

In comparation with the previous _docker-compose_ file, this one is pretty simple. Like I explained before, I had to
remove the networks. Also the volumes, and the ports that were in the _db_ service. And due to the tools I was using I
had to use an image for each service instead of having the instruction to build the image from the _dockerfile_. After
all this changes, the application is now ready to deploy.

*4.Deploy the application to _Kubernetes_*

After the installation of the tools, previously mentioned, the following commands were used, inside the folder _
Kubernetes_, it's necessary to run these commands where the _docker-compose_ file is:

```
> minikube start
(...)
Downloading Kubernetes v1.20.2 preload ...
(...)
Preparing kubernetes v1.20.2 on Docker 20.10.6 ...
(...)
Done!kubectl is now configured to use minikube cluster ...
```

_Minikube_ simulates the cluster that is needed in order for deploying.

```
>kompose convert

INFO kubernetes file "web-service.yaml" created
INFO kubernetes file "db-deployment.yaml" created
INFO kubernetes file "web-deployment.yaml" created
```

_Kompose_ is the tool that uses the _docker-compose_ file to create the configuration files for kubernetes. It was
created a configuration file for deployment for each service, _web_ and _db_. And a configuration file from the
application.

```
>kubectl apply -f web-service.yaml,db-deployment.yaml,web-deployment.yaml

service/web created
deployment.apps/db created
deployment.apps/web created
```

_Kubectl_ tool is needed in order to apply the configurations, that are in the files created with _Kompose_, to the
cluster. And the app is deployed. In order to see if the deploy was successful and everything is working correctly I
used another command:

```
>minikube dashboard
(...)
Launching proxy...
```
With this command we can access the _Kubernetes_ dashboard.

![kubernetes_dashboard](./images/5.png)

Both containers were deployed successfully, but is possible to access the application through the browser and for that I used, in a new terminal tab, the following command:

```
>kubectl port-forward web-6df865559c-kwwxw 8081:8080

Forwarding from 127.0.0.1:8081 -> 8080
Forwarding from [::1]:8081 -> 8080
(...)
Handling connection for 8081
(...)
```
These commands allow us to see the application in our browser, since we are forwarding to the port 8081, in our machine, the application that is
running in the pod, web-6df865559c-kwwxw, in port 8080.

Using _localhost:8081/tut-basic-ca2-0.0.1-SNAPSHOT/_:

![kubernetes_browser](./images/6.png)

After this step I found a problem since if I deleted the pod where the _db_ container was running, my application continued to show the information
from the database. This problem indicates that my application wasn't using the database from the container _db_. 
Unfortunately I'm still trying to understand what's happening and how to solve this problem.
Despite having been successful in the deployment of my two containers, this problem that I encountered, means that maybe my application
is running various services in the same pod, this goes against the purpose of _Kubernetes_.
But I'm currently working on solving this problem. 

## 4. *Docker vs Kubernetes* 

After this _ca_, I think of _Kubernetes_ more as a complement of _Docker_ rather than an alternative.
The main difference that I detected was the fact that _Docker_ can be used without _Kubernetes_ but
_Kubernetes_ needs a container runtime in order to orchestrate. In principle, it can work with any containerization technology, but it benefits from _Docker_.
Another fundamental difference is that _Kubernetes_ runs across a cluster while _Docker_ runs on a single node. 
In conclusion, the two of them are different tecnhologies that work well together for compilation and deployment of containerized applications. 



