# Class Assignment 1 Report - Version Control with Git

## 1. Analysis, Design and Implementation

### Git

Is a free and open source distributed version control system used from small to big projects. It has multiple workflows,
is super-fast and has a super efficient performance, cross-platform, and code changes can be easily tracked, is easy to
maintain and offers *Git GUI*, between other advantages. Is considered one of the best version control tools available
and currently is the most used one.

### Goals

The goal for this class assignment was to illustrate a simple git workflow. It was necessary to use the *Tutorial
React.js and Spring Data REST Application*, to do multiple commits and to publish various stable versions to the master
branch. So the initial version of this assignment was tagged as **v1.2.0.**. <br>
Several issues were created on Bitbucket in order to better organize the task.

### Assignment

**1. First check if you are on the master branch**

```console

git status

```

* This command shows the working tree status.

**2. Create a branch named "email-field" to implement the new feature**

```console
git checkout -b email-field
```

* The flag _-b_ creates a new branch with the designated name.

* If you want to check all the branches use _git branch_ .

* In the example above, _git checkout_, guaranties that after the branch is created we switch to it.

* An alternative would be:

```console
git branch email-field 
git checkout email-field
```

* This way, we first create the branch, _git branch_, and then we switch to it, _git checkout_ .

**3. After the implementation of the new feature, and the respective unit tests, commit the changes to the branch**

```console
git status 
git add .
git commit -m "Add tests for validations"
git push 
```

* With _git status_ we can see the files that were modified or deleted.
* It's possible to add every file or to choose which ones we want to commit, just by placing the name of the file next
  to _git add_ . In this case I used the git command followed by "." since I wanted to add all the files that were
  modified.
* In this case, since it's an individual repository, I didn't use the command _git pull_ , that would be necessary in a
  sharing repository since it's enables all the members to have the most recent version of the project.

**4. Merge the code from the branch into master and tag the stable version as v1.3.0**

```console
git checkout master
git merge email-field
git tag -a v1.3.0 -m "Stable version"
git push origin v1.3.0
```

* An alternative to _git merge_ would be a pull request through Bitbucket .
* With _git tag -a_ creates a tag object and assigns it to the last commit. The _-a_ flag makes an annotated tag, and
  the _-m_ flag enables you to add a message to it.

**5. Creation of a branch for fixing bugs related to the new added feature, named fix-invalid-email**

```console
git checkout -b fix-invalid-email
```

**6. After the implementation of new validations and respective tests, commit the changes into the new branch**

```console
git status 
git add .
git commit -m "Test email validation"
git push 
```

**7. Merge the new branch with master and tag the latest version**

```console
git checkout master
git merge fix-invalid-email
git tag -a v1.3.1 -m "Stable version"
git push origin v1.3.1
```

## 2. Analysis of an Alternative

There are various alternatives to *Git*, centralized or distributed, some of them are Mercurial, CVS and SVN. The
centralized version control uses server/client model, and the server contains all the history of the source code. Where
in the distributed version each client can have the same copy of source code. For this assignment the alternative I
chose was *Apache Subversion (SVN)*. 
*SVN* is a centralized version control system distributed under an open source
license. Some features of this software are:

* Directories are versioned;
* Copying, deleting, moving and renaming operations are also versioned;
* Supports atomic commits;
* Branching is not dependent upon the file size and this is a cheap operation. 

  **Benefits:**
  
    * Supports empty directories;
    * Easy to set up and administer;
    * Have better windows support as compared to Git;´
    * Has a benefit of good GUI tools like TortoiseSVN;
    * Integrates well with Windows, leading IDE and Agile tools. 

  **Disadvantages:**
  
    * Does not store the modification time of files;
    * Does not deal well with filename normalization;
    * Does not support signed revisions.

## 3. Implementation of the Alternative

For the implementation using *SVN*, I used one of *ISEP's* virtual servers. I consulted
various [tutorials](https://www.youtube.com/watch?v=Y9enCuIhwY8) and asked help to some group colleagues.

**1. Install SVN on the server and on your computer**


```console
sudo apt-get install subversion
```

* In my case, since I use Linux, to install it on my computer I had to run the same command.

**2. Set a new repository on the server**


```console
mkdir -p /svn/repository
```

* The server will be the only one who has the entire project, so this command allows to create a directory for the
  project.

```console
svnadmin create /svn/repository/tut-basic
```

* This command allows the creation of a repository.
* When running this command, a configuration file is generated automatically, which contains the repository permissions,
  we must edit this file, in this case using the text editor *VIM*:
  _vi /svn/repository/tut-basic/conf/svnserve.conf_ 
  Then the following lines should be uncommented:
  ```
  ...
  anon-access = read 
  anon-access = write
  ...
  password-db = passwd
  ```
* The first line allows anonymous users to read, the second gives writing permissions to authorized users, and the third
  line defines where the users passwords are stored.

* Next, we need to create a new user: 
_vi /svn/repository/tut-basic/conf/passwd_ 
  Specify the username and password:
  ```
  ...
  [users]
  username = password
  ```

**3. Start the server**

```console
svnserve -d -r /svn/repository
```

* The _-d_ flag is for daemon, _-r_ is to define the root directory.

**4. Import the project, that is in your computer, to the server**

```console
svn import ./tut-basic svn://vs159.dei.isep.ipp.pt/tut-basic -m "Import project to server"
```

* The server needs to be running.
* In this command, we first indicate the directory of the project (_tut-basic_), then the IP address of the server. The
  project is stored in the server and can be deleted from your personal computer.
* To get a copy from the project that is on the server you need to use the command:

  _svn checkout svn://vs159.dei.isep.ipp.pt/tut-basic_, which corresponds to the _clone_ command used in *Git*.

**5. Tag the stable version that is in the trunk**

* When using *SVN*, the structure of the repository is slightly different of a *Git* repository. 

  In *SVN* there are three main folders, *trunk* that equals the main branch in *git*, *tags* and *branches* . 
  
  The *tags* folder, stores a copy of the entire project, and the folder *branches* is where is stored that project that
  is being altered.
* To tag a stable version in the _trunk_ the command used is:

```console
svn copy svn://vs159.dei.isep.ipp.pt/tut-basic/trunk svn://vs159.dei.isep.ipp.pt/tut-basic/tags/v1.2.0 -m "v1.2.0"
```

* The _copy_ command creates a "snapshot" of the stable version in the *tags* folder. The v1.2.0 is the name of the
  folder in *tags*.

**6. Create a new branch to add a new feature**

```console
svn copy svn://vs159.dei.isep.ipp.pt/tut-basic/trunk svn://vs159.dei.isep.ipp.pt/tut-basic/branches/email-field -m "email-field"
```

* In this case it's also used the _copy_ command, but here the difference is that the project is stored in the 
  *branches* folder.
* To get this copy we need to run _svn update_ . This command must be used inside the folder that we want to update.

**7. Add and commit the changes that were made in the project**

```console
svn status
svn add <file name>
svn commit Employee.java -m "Add email field to Employee"
```

* In *SVN* the commands to add, commit and see the files that suffered modifications are the same as in *Git* .

**8. Merge the branch with trunk**

```console
svn merge ^/trunk 
```

* First the _trunk_ is merged into the new branch. The _^/_ is an alias for the root directory.

```console
svn commit -m "Merge trunk into email-field"
```

* Then we merge the branch _email-field_ into _trunk_ . First we switched to the _trunk_ and then run the command:

```console
svn merge --reintegrate ^/branches/email-field
``` 

* The _--reintegrate_ flag is used to bring changes from a feature branch into the feature branch's immediate ancestor
  branch. 
  
  Then commit the changes using _svn commit_ .

* The next steps in this class assignment can be done using the steps previously described (5 to 8). 
  
