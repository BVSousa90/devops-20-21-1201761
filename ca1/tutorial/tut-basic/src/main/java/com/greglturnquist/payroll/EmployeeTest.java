package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @DisplayName("Create new Employee")
    @Test
    void createEmployee(){
        Employee saphira = new Employee("Saphira","Blue","Dragon","Protect","bluedragon@eragon.com");
        assertNotNull(saphira);
    }

    @DisplayName("Failure creating: null name ")
    @Test
    void failureCreatingEmployee(){
        assertThrows(IllegalArgumentException.class, () -> new Employee(null,"Eragon","Half elf","Dragon Rider","eragonRider@.saphira.com"));
        }

    @DisplayName("Failure creating: null job ")
    @Test
    void failureCreatingEmployee1(){
        assertThrows(IllegalArgumentException.class, () -> new Employee("Srugtal","Eragon","Half elf",null,"eragonRider@.saphira.com"));
    }
    @DisplayName("Failure creating: empty email")
    @Test
    void failureCreatingEmployee2(){
        assertThrows(IllegalArgumentException.class, () -> new Employee("Srugtal","Eragon","Half elf","Dragon Rider"," "));
    }
    @DisplayName("Failure creating: empty description")
    @Test
    void failureCreatingEmployee3(){
        assertThrows(IllegalArgumentException.class, () -> new Employee("Srugtal","Eragon"," ","Dragon Rider","eragonRider@.saphira.com"));
    }
    @DisplayName("Failure creating: empty lastName")
    @Test
    void failureCreatingEmployee4(){
        assertThrows(IllegalArgumentException.class, () -> new Employee("Srugtal",""," ","Dragon Rider","eragonRider@.saphira.com"));
    }
    @DisplayName("Failure creating: wrong email format")
    @Test
    void failure_email(){
       assertThrows(IllegalArgumentException.class, () -> new Employee("Srugtal", "Eragon", "Half elf", "Dragon Rider", "eragon%.com"));
    }
}





