# Class Assignment 3 Report, Part 1 - Virtualization with Vagrant 

# 1. Analysis, Design and Implementation 

## Virtualization

Is the process of running a virtual instance of a computer system in a layer abstracted from the actual hardware, that allows the hardware elements of a single computer - processors, memory, storage and more - to be divided into multiple virtual computers, commonly called virtual machines (VMs). To the applications running on top of the virtual machine, it appears as if they are on their own dedicated machine. 
Each VM runs its own operanting system (OS).

If the virtual machine is stored on a virtual disk, this is often referred to as a disk image. A disk image may contain the files for a virtual machine to boot, or, it can contain any other specific storage needs. 

The program responsable for creating and running the VMs is _hypervisor_. Two classes exist, type one and type two. 
Type one, run guest virtual machines directly on a system's hardware, essentially behaving as an OS.
Type two, behave more like traditionl applications that can be started and stopped like a normal program. 
For this assignment the _hypervisor_ that was chosen was [_VirtualBox_](https://www.virtualbox.org/).


### Bennefits of Virtualization 

Virtualization brings several benefits to data center operators and service providers such as: 

> - Resource efficiency;
> - Easier management;
> - Minimal downtime;
> - Faster provisioning; 
> - Between others.


## Implementation 

The goal for this first part of the class assignment was to pratice with _VirtualBox_ using the same projects from the previous assignments. 

### *1.VM installation and configuration* 

The VM instalation and configuration was made during the class, so I will not repeat every step of the process. However I think some steps are relevant to mention. 

- It was necessary to add a second network adapter, host-only, this adapter enables a connection between the host and the guest machine by editing the network configuration file: _/etc/netplan/01-netcfg.yaml_.

- When editing the network configuration file in order to setup the IP, the IP that was choosen was _192.168.56.12_.

- THe _openssh-server_ was also installed in order to access the guest machine remotely. 

- It was also important to install some dependencies, like _git_ , _zip_, _curl_ and it was also installed a manager of tools, _SDK_. 
_Java_ and _Gradle_  and _Maven_ were also installed using _SDK_. 

### *2.Cloning the personal repository* 

Before cloning my personal repository, I accessed my _VM_ through my [host machine](./images/1.png).  
Using:

```console
ssh baa@192.168.56.12
password ...
```
My user name and password were defined with the setup of the _VM_.

I decided to clone my entire repository however it's possible to clone just the first two folders that correspond to the first class assignments. 
So to clone my repository to the guest machine, I just used _git clone git@bitbucket.org:BVSousa90/devops-20-21-1201761.git_. 

Now we can access the class assignments through the guest machine. 

### *3.Running class assignment 1*

First we need to go into the correct folder, using _cd devops-20-21-1201761/ca1/tutorial/tut-basic_.
Since the first assignment uses _Maven_ as a build tool, we start by using _mvn spring-boot:run_, to see the result we need to go to the URL: _https://192.168.56.12:8080/, and we can see the [result](./images/2.png). 


### *4.Running class assignment 2, part 1*

This part of the class assignment uses _Gradle_ and is a little chat application. In order to execute this, in the script _build.gradle_ that is in _ca2/Part_1_, in my host machine, the task _runClient_ needs to be altered, instead of receving localhost as one of the arguments, now receives my [IP](./images/3.png). In my guest machine, I need to access _ca2/Part_1/gradle_basic_demo_ and run the command _./gradlew runServer_. And in my host machine I run the command _./gradlew runClient_, and the chat box opens automatically. 


### *4.1.Running part 2, of ca2*

Like the first part of this assignment, this one also uses _Gradle_. On my guest machine, going into _ca2/Part_2/demo_, and running the command _./gradlew bootRun_, we can open a browse page to confirm the result, using [http://192.168.56.12:8080](./images/4.png). And it shows the expected result. 











