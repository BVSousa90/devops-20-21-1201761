# Class Assignment 3 Report,Part 2 - Virtualization with Vagrant

# 1.Analysis, Desing and Implementation

A brief introduction to _Virtualization_ was already made in [_Part_1_](https://bitbucket.org/BVSousa90/devops-20-21-1201761/src/master/ca3/Part_1/) of this assignment.

## Vagrant

Is an open-source tool for building and managing virtual machine environments in a single workflow.
With a focus on automation, Vagrant lowers development environment setup time, increases production and flexibility.
Vagrant will isolate dependencies and their configuration within a single disposable, consistent environment, without sacrificing any of the tools that are used in a project.

### Vagrant File

The main function of this file is to describe the type of machine required for a project, and how to configure and provision these machines.

_Vagrant_ is supposed to run with one _Vagrantfile_ per project and this is supposed to be commited to version control. This allows other developers in the same project to check out the code.

The syntax of this file is _Ruby_ but it's not necessary a language knowledge in order to make modifications to the file.

So we can delete our _VM_ since we have our _Vagrantfile_ and other important files, we can share it and others can recreate our work environment.

### Providers

Vagrant has the ability to manage various types of machines.
Alternative providers to _VirtualBox_, _Hyper-V_ and _Docker_, can offer different features.
Installation of other providers is done via the _Vagrant_ plugin system.

## Implementation

The goal for this part of the assignment was to use _Vagrant_ to setup a virtual environment to execute the tutorial _spring boot application, gradle "basic" version_, developed in [_CA2,Part2_](https://bitbucket.org/BVSousa90/devops-20-21-1201761/src/master/ca2/Part_2/).

### _1.Import the initial solution_

First we start by downloading or cloning the [repository](https://bitbucket.org/BVSousa90/devops-20-21-1201761/src/master/ca2/Part_2/).

In your terminal chose the folder where you want the project to be, after cloning the project, copy the _Vagrantfile_ and remove the folder.

```console
> cd IdeaProjects/devops-20-21-1201761/ca3/Part_2/
git clone https://bitbucket.org/atb/vagrant-multi-spring-tut-demo.git

> ls
README.md  vagrant-multi-spring-tut-demo

> cd vagrant-multi-spring-tut-demo
readme.md  Vagrantfile

> cp /home/barbarasousa/IdeaProjects/devops-20-21-1201761/ca3/Part_2/vagrant-multi-spring-tut-demo/Vagrantfile  /home/barbarasousa/IdeaProjects/devops-20-21-1201761/ca3/Part_2

> cd IdeaProjects/devops-20-21-1201761/ca3/Part_2
README.md  Vagrantfile  vagrant-multi-spring-tut-demo

> rm -rf vagrant-multi-spring-tut-demo

>ls
README.md  Vagrantfile

```

### _2.Edit Vagrantfile in order to use the application from ca2, part2_

We need to [change some lines](./images/1.png) in the _Vagrantfile_ for the application from ca2, part2 can run.

We changed the lines in order to clone the right repository. To change the directory where the gradle spring application is located and we changed the name of the _war_ file, because it was different from the one in the project.

### _3.Change ca2, part 2 application in order to use H2 server in the VM_

For this process, I used the [repository](https://bitbucket.org/atb/tut-basic-gradle/src/master/), in order to do the necessary changes to my project.

The following files were modified:

> - build.gradle
> - applications.properties
> - app.js
> - ServletInitializer.java

### _4.Running the VM's_

Make sure you are in the same folder as the _Vagrantfile_ and run the command _vagrant status_, too see the current states of the machine.

```console
> vagrant status

Current machine states:

db                        poweroff (virtualbox)
web                       poweroff (virtualbox)

This environment represents multiple VMs. The VMs are all listed
above with their current state. For more information about a specific
VM, run `vagrant status NAME`.
```

In order to start the machines, we can use _vagrant up db_ and _vagrant up web_ or we can start both at the same time usign just _vagrant up_.

```console
>vagrant status

Current machine states:

db                        running (virtualbox)
web                       running (virtualbox)

This environment represents multiple VMs. The VMs are all listed
above with their current state. For more information about a specific
VM, run `vagrant status NAME`.
```

Opening the H2 console through _http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console_, is possible to see the login console, in order to login we need to use the URL _`jdbc:h2:tcp://192.168.33.11:9092/./jpadb`_.
Also in the host you can open the sring web application (using http://localhost:8080/demo-0.0.1-SNAPSHOT ) and the result is the expected one.

# 2. Analysis of an Alternative

To choose an alternatives to _VirtualBox_ was quite a challenge, the main providers of _Vagrant_ were, as already mention before, _VirtualBox, VMware, Docker and Hyper-V_.
_VMware_ requires a plugin in order to use _Vagrant_ that is necessary to pay. So it was not an option. _Hyper-V_ is only available for _Windows_ and my OS is _Linux_.
Since _Docker_ is the topic of the next classes and class assignments, I didn't want to use it as an alternative for this assignment.

## AWS - Amazon Web Services - Cloud Computing Services

Doing some research I came across _AWS_. These could computing web services provide a variety of a basic abstract technical infrastruture and distruibuted computing building tools. One of these servies is _Amazon Elastic Compute Cloud_, wich allows users to have at their disposala virtual cluster of computers, available at all times. Their version of virtual computers emulates most of the attributes of a real computer.

# 3.Implementation of the Alternative

At the moment, I didn't finish the implementation of the alternative.
It's been quite a challenge to learn how to use _AWS_. But I will coninue to learn in order to implement the alternative until the end of the semester.
---------------//---------------------------------------------------------
Coming back to this part of the class assignment, I learned how to launch an instance, on the amazon servers, using the vagrantfile, however I didn't complete this alternative since I couldn't discover on time, how to create two VM equivalents using the amazon services. 
I will leave here the process of launching an instance.

*Amazon Services*

After creating an account on amazon services, I installed the necessary plugin in order to use vagrant with _aws_.
Opening the command line:

```
vagrant plugin install vagrant-aws
vagrant box add dummy https://github.com/mitchellh/vagrant-aws/raw/master/dummy.box
```

The vagrant box command, was to built the vagrant box in order for the provider to use it, in this case the provider being amazon services. 

_1.Vagrantfile_

In the folder alternative, I used _vagrant init_ to create a template of the _Vagrantfile_, this file is in the folder and in the repository. 

After everything was set up, it was necessary to run a simple command:

```
vagrant up --provider=aws
```

![instance-terminal](./alternative/1.png)

![instance](./alternative/2.png)

![instance-1](./alternative/3.png)


After learning what was important to use in the vagrantfile, this process wasn't difficult however I had a lot of struggles trying to do the assignment using this alternative and I wasn't able to reach a conclusion and finish the implementation of the alternative.


